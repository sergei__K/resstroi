<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use backend\widgets\AndNav;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use backend\models\Page;
use backend\models\StaticTextItem;
use backend\models\UserRole;
use backend\models\Base;
use backend\models\PageItem;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <!--<script>//var INLINE_SVG_REVISION = <? //=filemtime(\Yii::getAlias('@frontend/web/svg.html'))?>;</script>-->

        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?= Html::csrfMetaTags() ?>

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/menu.css">
        <link rel="stylesheet" href="css/lightbox.css">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="manifest" href="/site.webmanifest">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
    </head>
    <body class="body">
    <?php $this->beginBody() ?>

    <?

    $isAbout = \Yii::$app->request->url == $this->params[Page::PAGE_PREFIX . Page::ABOUT_COMPANY]['linkOut'];
    $isContacts = \Yii::$app->request->url == $this->params[Page::PAGE_PREFIX . Page::CONTACTS_PAGE]['linkOut'];
    $isDelivery = \Yii::$app->request->url == $this->params[Page::PAGE_PREFIX . Page::DELIVERY]['linkOut'];
    $isMain = \Yii::$app->controller->id == 'site';
    $isCategory = \Yii::$app->controller->id == 'category';
    $isDesign = \Yii::$app->controller->id == 'design-collection';
    $isReview = \Yii::$app->controller->id == 'review';
    $isError = \Yii::$app->controller->action->id == 'error';
    //$isProduct = \Yii::$app->controller->id == 'product';
    //$isView = \Yii::$app->controller->action->id == 'view';
    ?>

    <header>
        <div class="zerogrid">
            <div class="wrap-header">
                <div id='cssmenu' class="align-center">
                    <ul>

                        <li class="active"><a href='/'><span><img src="img\logo.jpg" width="58">Resstoroi</span></a>
                        </li>

                        <li class=' has-sub'><a href=''><span>Категории</span></a>
                            <ul>
                                <li class='has-sub'><a href='#'><span>Стройматериалы</span></a>
                                </li>
                                <li class='has-sub'><a href='#'><span>Техника</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div style="margin-bottom: 30px;">
                        <img src="img/2.jpg" alt="" width="1200">
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?= $content ?>
    <footer data-bgColor="pink">
        <div class="zerogrid">
            <div class="wrap-footer">
                <div class="row">
                    <div class="col-1-2">
                        <div class="wrap-col">
                            <p> Resstroi
                        </div>
                    </div>
                    <div class="col-1-2">
                        <div class="wrap-col">
                            <ul class="bottom-social f-right">
                                <li><a href="#"><img src="img\google.png"></a></li>
                                <li><a href="#"><img src="img\instagram.png"></a></li>
                                <li><a href="#"><img src="img\pinterest.png"></a></li>
                                <li><a href="#"><img src="img\facebook.png"></a></li>
                                <li><a href="#"><img src="img\twitter.png"></a></li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </body>
    </html>
<?php $this->endPage() ?>