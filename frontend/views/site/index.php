<?php

use backend\models\StaticTextItem;
use yii\helpers\Url;
use backend\models\Page;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
\frontend\assets\AppAsset::register($this);

?>


<? if (!empty($banners)): ?>
    <div class="main-slider swiper-container js-main-slider">
        <div class="main-slider__wrapper swiper-wrapper">
            <? foreach ($banners as $banner): ?>
                <? /* @var $banner \backend\models\Banner */ ?>
                <div class="slide swiper-slide">
                    <img class="slide__image swiper-lazy" alt="<?= $banner->name ?>"
                         data-src="<?= $banner->getSRCPhoto(['suffix' => '_big']) ?>">
                    <div class="slide__content">
                        <div class="slide__cell">
                            <? if (!empty($banner->name)): ?>
                                <div class="slide__title"><?= $banner->name ?></div>
                            <? endif; ?>
                            <? if (!empty($banner->text)): ?>
                                <div class="slide__caption"><?= $banner->text ?></div>
                            <? endif; ?>
                            <? if (!empty($banner->link) || !empty($banner->link_text)): ?>
                                <div class="slide__button">
                                    <a class="button button_color_dark"
                                       href="<?= $banner->link ?>"><?= $banner->link_text ?></a>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="main-slider__pagination js-main-slider__pagination"></div>
        <div class="main-slider__button js-main-slider__prev">
            <svg class="main-slider__icon">
                <use xlink:href="#icon-arrow-down"></use>
            </svg>
        </div>
        <div class="main-slider__button main-slider__button_next js-main-slider__next">
            <svg class="main-slider__icon">
                <use xlink:href="#icon-arrow-down"></use>
            </svg>
        </div>
    </div>
<? endif; ?>

    <div class="wrapper wrapper_nopad">
        <section class="advantages advantages_moved">
            <div class="advantages__container">
                </div>
            </div>
        </section>
        <? if (!empty($collections)): ?>
            <section class="page__section">
                <div class="teaser">
                    <? foreach ($collections as $collection): ?>
                        <? /* @var $collection \backend\models\Category */ ?>
                        <? $count = $collection->getProducts()->count() ?>

                        <a href="<?= $collection->linkOut ?>" class="teaser__item">
                            <div class="teaser__image"
                                 style="background-image: url('<?= $collection->getSRCPhoto(['suffix' => '_mid']) ?>')">
                                <div class="teaser__wrapper">
                                    <div class="teaser__title"><?= $collection->name ?></div>
                                    <div class="teaser__info">
                                        <span class="teaser__label"><?= \backend\models\Base::inclination($count) ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <? endforeach; ?>
                </div>
            </section>
        <? endif; ?>
    </div>
<? if (!empty($products)): ?>
    <div class="wrapper">
        <section class="page__section-l">
            <h2 class="title title_centered title_top"><center>товары</center></h2>
            <div class="items-slider">
                <div class="items-slider__container swiper-container js-items-slider">

                        <? foreach ($products as $product): ?>
                            <? /* @var $product \backend\models\Product */ ?>
                        <div class="col-xs-4">
                                <a href="<?= $product->linkOut ?>" class="card">
                                    <? if ($product->flags > 0): ?>
                                        <div class="card__badge">
                                            <?= $product->AllFlagsAsArray()[$product->flags] ?>
                                        </div>
                                    <? endif; ?>
                                    <img class="card__image swiper-lazy" src="<?=  $product->getSRCPhoto(['suffix' => '_md', 'index' => 0]) ?>" alt="<?= $product->name ?>">
                                    <div class="card__content"><br>
                                        <span class="card__title"><?= $product->name ?></span>
                                        <div class="card__price"><?= number_format($product->cost, 0, '', ' ') ?>&nbsp;&#8381;</div>
                                    </div>
                        </div>
                        </a>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="items-slider__button js-items-slider__prev">
                    <svg class="items-slider__icon">
                        <use xlink:href="#icon-arrow-down"></use>
                    </svg>
                </div>
                <div class="items-slider__button items-slider__button_next js-items-slider__next">
                    <svg class="items-slider__icon">
                        <use xlink:href="#icon-arrow-down"></use>
                    </svg>
                </div>
                <div class="slider-pagination slider-pagination_bottom js-items-slider__pagination"></div>
            </div>
        </section>
    </div>
<? endif; ?>
<? if (false): ?>

    <section class="section section_decorated">
        <div class="section__wrapper">
            <? if (!empty($furniture)): ?>
                <div class="section__column section__column_last">
                    <div class="rack">
                        <h2 class="title title_top"><?= $furniture->name ?></h2>
                        <div class="rack__header">
                            <?= $furniture['texts'][StaticTextItem::FURNITURE_TEXT]['text'] ?>
                        </div>
                        <? if (!empty($furniturePhotos)): ?>
                            <div class="rack__content">
                                <!-- data-images - это строка в uriencoded json объекта с данными картинок для галереи
                                https://codepen.io/HummerHead87/pen/oEgZyy?editors=1011
                                ВАЖНО: первые 2 картинки должны быть большими вариантами картинок в элементе img.small-gallery__image -->

                                <div class="small-gallery js-small-gallery"
                                     data-images="<?= urlencode(json_encode($furniturePhotos['data'])) ?>">

                                    <a class="small-gallery__item js-small-gallery__item"
                                       href="<?= $furniturePhotos['data'][0]['src'] ?>">
                                        <img class="small-gallery__image"
                                             src="<?= $furniturePhotos['preview'][0]['src_sm'] ?>"
                                             alt="<?= $furniturePhotos['preview'][0]['title'] ?>">
                                        <div class="small-gallery__mask">
                                            <svg class="small-gallery__icon">
                                                <use xlink:href="#icon-zoom"></use>
                                            </svg>
                                        </div>
                                    </a>

                                    <a class="small-gallery__item js-small-gallery__item"
                                       href="<?= $furniturePhotos['data'][1]['src'] ?>">
                                        <img class="small-gallery__image"
                                             src="<?= $furniturePhotos['preview'][1]['src_sm'] ?>"
                                             alt="<?= $furniturePhotos['preview'][1]['title'] ?>">
                                        <div class="small-gallery__mask">
                                            <svg class="small-gallery__icon">
                                                <use xlink:href="#icon-zoom"></use>
                                            </svg>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <? endif; ?>
                        <div class="rack__bottom">
                            <a class="button button_color_dark"
                               href="<?= $this->params['contacts'][\backend\models\PageItem::CONTACTS_INSTA]['text'] ?>">
                                <svg class="icon icon_inline">
                                    <use xlink:href="#icon-instagram"></use>
                                </svg>
                                Смотреть
                            </a>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </section>
<? endif; ?>

<? if (!empty($staticText[StaticTextItem::TEXT]['text'])): ?>
    <div class="wrapper">
    </div>
<? endif; ?>